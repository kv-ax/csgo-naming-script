# CSGO Naming Script

Rename your weapons to a max of 21 characters (from 20 in-client), as well as forbidden unicode characters that are sanitized in-client.

### Setting up

Copy and rename `example.config.js` to `config.js` in a text editor of your choice.

Change the following to their respective values in the quotation marks corresponding with them.

```js
accountName
password
itemID
nametagID
desiredName
```

You can find the item ID and the nametag ID on a site such as [CSGO Exchange](https://csgo.exchange)

### Installation

The script requires [Node.js](https://nodejs.org/) v4.1.1+ to run.

Install the dependencies and run the rename script.

```sh
$ cd csgo-naming-script
$ npm install
$ npm run rename
```