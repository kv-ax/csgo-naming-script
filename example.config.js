module.exports = {
	accountName: "",	// Your steam account's name
	password: "",		// Your steam account's password
	item: "",			// Your gun's ID
	nametag: "",		// Your nametag's ID
	name: "",			// The desired name for the item
}
